import type Category from "@/types/Category";
import http from "./axios";
function getCategorys() {
  return http.get("/category");
}
function saveCategory(category: Category) {
  return http.post("/category", category);
}

function updateCategory(id: number, category: Category) {
  return http.patch(`/category/${id}`, category);
}

function deleteCategory(id: number) {
  return http.delete(`/category/${id}`);
}

export default { getCategorys, saveCategory, updateCategory, deleteCategory };
