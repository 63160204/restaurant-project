import http from "./axios";
import type OrderDetail from "@/types/OrderDetail";

function getOrderDeail() {
  return http.get("/order-details");
}

function getOrderDetailByTableId(id: number) {
  return http.get(`/order-details/tables/${id}`);
}

function updateOrderDetail(id: number, orderDeatil: OrderDetail) {
  return http.patch(`/order-details/${id}`, orderDeatil);
}

export default {
  getOrderDeail,
  updateOrderDetail,
  getOrderDetailByTableId,
};
