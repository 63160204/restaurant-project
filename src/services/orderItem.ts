import type OrderDetail from "@/types/OrderDetail";
import http from "./axios";
import type OrderItem from "@/types/OrderItem";

function getOrderItem() {
  return http.get("/order-item");
}

function getOrderItemByTableId(id: number) {
  return http.get(`/order-items/tables/${id}`);
}

function saveOrderItem(orderItem: {
  orderId: number;
  orderDetail: { productId: number; ord_detail_amount: number }[];
}) {
  return http.post("/order-items", orderItem);
}

function updateOrderItem(id: number, orderItem: OrderDetail) {
  return http.patch(`/order-items/${id}`, orderItem);
}

function deleteOrderItem(id: number) {
  return http.delete(`/order-items/${id}`);
}

export default {
  getOrderItem,
  saveOrderItem,
  updateOrderItem,
  deleteOrderItem,
  getOrderItemByTableId,
};
