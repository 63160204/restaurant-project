import type Orders from "@/types/Order";
import http from "./axios";
function getOrders() {
  return http.get("/orders");
}

function getOrderByTableId(id: number) {
  return http.get(`/orders/tables/${id}`);
}

function saveOrders(order: Orders) {
  return http.post("/orders", order);
}

function updateOrders(id: number, order: Orders) {
  return http.patch(`/orders/${id}`, order);
}

function deleteOrders(id: number) {
  return http.delete(`/orders/${id}`);
}

export default {
  getOrders,
  saveOrders,
  updateOrders,
  deleteOrders,
  getOrderByTableId,
};
