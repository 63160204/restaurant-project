import type Product from "@/types/Product";
import http from "./axios";
function getProducts(params: any) {
  return http.get("/products", { params: params });
}

function getAllProduct(params: any) {
  return http.get("/products", { params: params });
}

function getProductsByCategory(category: number, keyword?: string) {
  return http.get(`/products/category/${category}?keyword=${keyword}`);
}

function saveProducts(product: Product & { files: File[] }) {
  const formDate = new FormData();
  formDate.append("name", product.name);
  formDate.append("price", `${product.price}`);
  formDate.append("categoryId", `${product.categoryId}`);
  formDate.append("file", product.files[0]);
  return http.post("/products", formDate, {
    headers: {
      "Content-Type": "multipart/form-date",
    },
  });
}

function updateProducts(id: number, product: Product & { files: File[] }) {
  const formDate = new FormData();
  formDate.append("name", product.name);
  formDate.append("price", `${product.price}`);
  formDate.append("categoryId", `${product.categoryId}`);
  formDate.append("file", product.files[0]);
  return http.patch(`/products/${id}`, formDate, {
    headers: {
      "Content-Type": "multipart/form-date",
    },
  });
}

function deleteProducts(id: number) {
  return http.delete(`/products/${id}`);
}

export default {
  getProducts,
  saveProducts,
  updateProducts,
  deleteProducts,
  getAllProduct,
  getProductsByCategory,
};
