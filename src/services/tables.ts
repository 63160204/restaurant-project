import type Tables from "@/types/Table";
import http from "./axios";
function getTables(params: any) {
  return http.get("/tables", { params: params });
}

function getAllTables(params: any) {
  return http.get("/tables", { params: params });
}
function saveTables(table: Tables) {
  return http.post("/tables", table);
}

function updateTables(id: number, table: Tables) {
  return http.patch(`/tables/${id}`, table);
}

function deleteTables(id: number) {
  return http.delete(`/tables/${id}`);
}

export default {
  getTables,
  saveTables,
  updateTables,
  deleteTables,
  getAllTables,
};
