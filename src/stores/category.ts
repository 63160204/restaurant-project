import { ref } from "vue";
import { defineStore } from "pinia";
import type Category from "@/types/Category";
import categoryService from "@/services/category";

export const useCategoryStore = defineStore("category", () => {
  const checkDialog = ref();
  const deleteDialog = ref(false);
  const dialog = ref(false);
  const categorys = ref<Category[]>([]);
  const editedCategory = ref<Category>({
    category_name: "",
  });

  async function getCategorys() {
    try {
      const res = await categoryService.getCategorys();
      categorys.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function saveCategory() {
    try {
      if (editedCategory.value.category_id) {
        const res = await categoryService.updateCategory(
          editedCategory.value.category_id,
          editedCategory.value
        );
      } else {
        const res = await categoryService.saveCategory(editedCategory.value);
      }
      dialog.value = false;
      await getCategorys();
    } catch (e) {
      console.log(e);
    }
  }

  async function deleteCategory(id: number) {
    try {
      const res = await categoryService.deleteCategory(id);
      await getCategorys();
    } catch (e) {
      console.log(e);
    }
  }

  function editCategory(category: Category) {
    editedCategory.value = JSON.parse(JSON.stringify(category));
    dialog.value = true;
  }
  return {
    getCategorys,
    categorys,
    editedCategory,
    editCategory,
    deleteCategory,
    saveCategory,
    checkDialog,
    deleteDialog,
  };
});
