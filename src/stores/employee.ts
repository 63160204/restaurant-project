import { ref } from "vue";
import { defineStore } from "pinia";
import type Employee from "@/types/Employee";
import employeeService from "@/services/employee";

export const useEmployeeStore = defineStore("employee", () => {
  const dialogAdd = ref(false);
  const dialogEdit = ref(false);
  const dialogDelete = ref(false);
  const deleteemployee = ref();
  const editedemployee = ref<Employee>({
    name: "",
    surname: "",
    tel: "",
    jobtitle: "",
  });
  const employee = ref<Employee[]>([]);

  async function getEmployees() {
    try {
      const res = await employeeService.getEmployees();
      employee.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function AddEmployee() {
    try {
      const res = await employeeService.saveEmployee(editedemployee.value);
    } catch (e) {
      console.log(e);
    }
    dialogAdd.value = false;
    clearAdd();
    await getEmployees();
  }

  async function saveEmployee() {
    try {
      if (editedemployee.value.employee_id) {
        const res = await employeeService.updateEmployee(
          editedemployee.value.employee_id,
          editedemployee.value
        );
      } else {
        console.log(editedemployee.value);
        const res = await employeeService.saveEmployee(editedemployee.value);
      }
      await getEmployees();
    } catch (e) {
      console.log(e);
    }
  }

  function editEmployee(employee: Employee) {
    editedemployee.value = JSON.parse(JSON.stringify(employee));
    dialogEdit.value = true;
  }

  function clearAdd() {
    editedemployee.value = {
      name: "",
      surname: "",
      tel: "",
      jobtitle: "",
    };
  }

  async function deleteEmployee(id: number) {
    try {
      const res = await employeeService.deleteEmployee(id);
      await getEmployees();
    } catch (e) {
      console.log(e);
    }
  }

  return {
    employee,
    getEmployees,
    dialogAdd,
    dialogEdit,
    dialogDelete,
    editEmployee,
    deleteEmployee,
    saveEmployee,
    AddEmployee,
    deleteemployee,
    editedemployee,
    clearAdd,
  };
});
