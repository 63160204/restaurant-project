import { ref, computed } from "vue";
import { defineStore } from "pinia";
// import { useUserStore } from "./user";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";
import auth from "@/services/auth";
import router from "@/router";

export const useLoginStore = defineStore("login", () => {
  const loadingStore = useLoadingStore();
  // const userStore = useUserStore();
  const messageStore = useMessageStore();
  const loginName = ref("");
  const pageTitle = ref();
  const openMain = ref(true);

  const isLogin = computed(() => {
    return loginName.value !== "";
  });

  // const log = ref(true);
  // const main = ref(false);

  // const getUsers = () => {
  //   const usersString = localStorage.getItem("user");
  //   if (!usersString) return null;
  //   const user = JSON.parse(usersString ?? "");
  //   return user;
  // };

  // const isLogin = () => {
  //   const user = localStorage.getItem("user");
  //   if (user) {
  //     console.log(user);
  //     main.value = true;
  //     log.value = false;
  //     return true;
  //   }
  //   return false;
  // };

  const login = async (username: string, password: string): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      console.log({ username, password });
      const res = await auth.login(username, password);
      console.log(res);
      localStorage.setItem("user", JSON.stringify(res.data.user));
      localStorage.setItem("token", res.data.access_token);
      loginName.value = username;
      localStorage.setItem("loginName", username);
      // const user = JSON.parse(localStorage.getItem("user")!);
      console.log("success");
      router.push("/cashier");
    } catch (err) {
      console.log(err);
      messageStore.showMessage("username หรือ password ไม่ถูกต้อง");
    }
    loadingStore.isLoading = false;
  };

  // const login = async (username: string, password: string): Promise<void> => {
  //   loadingStore.isLoading = true;
  //   try {
  //     const res = await auth.login(username, password);
  //     localStorage.setItem("user", JSON.stringify(res.data.user));
  //     localStorage.setItem("token", res.data.access_token);
  //     console.log(res);
  //     router.push("/cashier");
  //     isLogin();
  //   } catch (e) {
  //     messageStore.showError("Username หรือ Password ไม่ถูกต้อง");
  //   }
  //   loadingStore.isLoading = false;
  // };

  const logout = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    loginName.value = "";
    router.push("/login");
  };

  const loadData = () => {
    loginName.value = localStorage.getItem("loginName") || "";
    // pageTitle.value = localStorage.getItem("pageTitle") || "";
  };

  // const showTitle = (titlei: string) => {
  //   localStorage.setItem("pageTitle", titlei);
  //   pageTitle.value = localStorage.getItem("pageTitle");
  // };

  return {
    loginName,
    isLogin,
    login,
    logout,
    loadData,
    openMain,
  };
});
