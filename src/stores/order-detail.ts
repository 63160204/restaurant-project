import { defineStore } from "pinia";
import { ref } from "vue";
import orderDetailService from "@/services/orderDetail";
import type OrderDetail from "@/types/OrderDetail";

export const useOrderDetailStore = defineStore("orderDetail", () => {
  const orderDetail = ref<OrderDetail[]>([]);

  async function getOrderDetail(id: number) {
    try {
      const res = await orderDetailService.getOrderDetailByTableId(id);
      orderDetail.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function getAllOrderDetail() {
    try {
      const res = await orderDetailService.getOrderDeail();
      orderDetail.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  // async function updateStatus() {
  //   try {
  //     const res = await statusService.updateStatus(
  //       editedStatus.value!.status_id!,
  //       editedStatus.value!
  //     );
  //     await getOrderDetail();
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }

  return {
    getOrderDetail,
    orderDetail,
    getAllOrderDetail,
  };
});
