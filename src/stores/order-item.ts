import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import orderItemService from "@/services/orderItem";
import { useOrderStore } from "./order";
import type OrderItem from "@/types/OrderItem";

export const useOrderItemStore = defineStore("orderItem", () => {
  const useOrder = useOrderStore();
  const orderItem = ref<OrderItem[]>([]);

  const orderList = ref<{ product: Product; amount: number; sum: number }[]>(
    []
  );
  const orderLists = ref<{ product: Product; amount: number; sum: number }[]>(
    []
  );

  async function getOrderItem() {
    try {
      const res = await orderItemService.getOrderItem();
      orderList.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function getOrderItemByTableId(tableId: number) {
    try {
      const res = await orderItemService.getOrderItemByTableId(tableId);
      orderItem.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  function addProduct(item: Product) {
    for (let i = 0; i < orderList.value.length; i++) {
      if (orderList.value[i].product.product_id === item.product_id) {
        orderList.value[i].amount++;
        orderList.value[i].sum = orderList.value[i].amount * item.price;
        console.log(orderList.value[i].amount);
        return;
      }
    }
    orderList.value.push({ product: item, amount: 1, sum: 1 * item.price });
  }

  function extendlist() {
    for (let i = 0; i < orderList.value.length; i++) {
      for (let j = 0; j < orderList.value[i].amount; j++) {
        orderLists.value.push({
          product: orderList.value[i].product,
          amount: 1,
          sum: 1 * orderList.value[i].product.price,
        });
      }
    }
  }

  function deleteProduct(index: number) {
    orderList.value.splice(index, 1);
  }

  const sumAmount = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].amount;
    }
    return sum;
  });
  const sumPrice = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].sum;
    }
    return sum;
  });

  function increaseAmount(index: number) {
    orderList.value[index].amount++;
    orderList.value[index].sum =
      orderList.value[index].amount * orderList.value[index].product.price;
  }

  function decreaseAmount(index: number) {
    if (orderList.value[index].amount > 1) {
      orderList.value[index].amount--;
      orderList.value[index].sum =
        orderList.value[index].amount * orderList.value[index].product.price;
    }
  }

  function clearOrder() {
    orderList.value = [];
  }

  async function openOrderItem(orderId: number) {
    orderId = useOrder.orderByTable?.order_id!;
    const orderDetail = orderLists.value.map(
      (item) =>
        <
          {
            productId: number;
            ord_detail_amount: number;
          }
        >{
          productId: item.product.product_id,
          ord_detail_amount: 1,
        }
    );
    const orderItem = {
      orderDetail: orderDetail,
      orderId: orderId,
    };
    try {
      const res = await orderItemService.saveOrderItem(orderItem);
      clearOrder();
    } catch (e) {
      console.log(e);
    }
  }

  return {
    orderList,
    openOrderItem,
    addProduct,
    decreaseAmount,
    deleteProduct,
    increaseAmount,
    sumAmount,
    sumPrice,
    clearOrder,
    extendlist,
    getOrderItem,
    getOrderItemByTableId,
    orderItem,
  };
});
