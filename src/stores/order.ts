import { ref } from "vue";
import { defineStore } from "pinia";
import type Orders from "@/types/Order";
import orderService from "@/services/orders";
import { useLoadingStore } from "./loading";

export const useOrderStore = defineStore("order", () => {
  const loadingStore = useLoadingStore();
  const orderId = ref(3);
  const dialogAdd = ref(false);
  const orderByTable = ref<Orders>();
  const orderByTable2 = ref<Orders>();
  const editedOrder = ref<Orders>();
  const orders = ref<Orders>();

  async function getOrders() {
    try {
      const res = await orderService.getOrders();
      orders.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function getOrderByTable(id: number) {
    loadingStore.isLoading = true;
    try {
      const rec = await orderService.getOrderByTableId(id);
      orderByTable.value = rec.data;
      orderByTable2.value = rec.data;
      console.log(orderByTable.value);
      // orderId.value =
      //   orderByTable.value[orderByTable.value.length - 1].order_id!;
      // return orderByTable.value;
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function addOrder(tableId: number) {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.saveOrders({ tableId });
      console.log(res);
    } catch (e) {
      console.log(e);
    }
    dialogAdd.value = false;
    clearAdd();
    await getOrders();
    loadingStore.isLoading = false;
  }

  function clearAdd() {
    editedOrder.value = {};
  }

  async function updateOrder(orderId: number) {
    try {
      const res = await orderService.updateOrders(orderId, orderByTable.value!);
      console.log(orderByTable.value);
    } catch (e) {
      console.log(e);
    }
  }

  return {
    clearAdd,
    editedOrder,
    dialogAdd,
    addOrder,
    orders,
    getOrders,
    getOrderByTable,
    orderByTable,
    orderId,
    updateOrder,
    orderByTable2,
  };
});
