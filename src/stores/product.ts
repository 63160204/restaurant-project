import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";

export const useProductStore = defineStore("product", () => {
  const loadingStore = useLoadingStore();
  const page = ref(1);
  const take = ref(10);
  const keyword = ref("");
  const lastPage = ref(0);
  const category = ref(1);
  const dialogQRCode = ref(false);
  const dialogAdd = ref(false);
  const dialogEdit = ref(false);
  const dialogDelete = ref(false);
  const deleteproduct = ref();
  const getByCatKeyword = ref("");

  const editedproduct = ref<Product & { files: File[] }>({
    name: "",
    price: 0,
    categoryId: 1,
    img: "no_img_available.jpg",
    files: [],
  });
  const products = ref<Product[]>([]);

  watch(keyword, async (newKeyword, oldKeyword) => {
    await getProducts();
  });

  watch(getByCatKeyword, async (newByCat, oldByCat) => {
    await getProductsByCategory(category.value, getByCatKeyword.value);
  });

  watch(page, async (newPage, oldPage) => {
    await getProducts();
  });

  watch(lastPage, async (newLastPage, oldLastPage) => {
    if (newLastPage < page.value) {
      page.value = 1;
    }
  });

  watch(category, async (newCategory, oldCategory) => {
    getByCatKeyword.value = "";
    await getProductsByCategory(newCategory, getByCatKeyword.value);
  });

  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts({
        page: page.value,
        take: take.value,
        keyword: keyword.value,
      });
      products.value = res.data.data;
      lastPage.value = res.data.lastPage;
      console.log(products.value);
    } catch (e) {
      console.log(e);
    }
  }

  async function getProductsByCategory(category: number, keyword?: string) {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProductsByCategory(category, keyword);
      products.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function getAllProducts() {
    try {
      const res = await productService.getAllProduct({
        keyword: keyword.value,
      });
      products.value = res.data.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function AddProduct() {
    try {
      const res = await productService.saveProducts(editedproduct.value);
    } catch (e) {
      console.log(e);
    }
    dialogAdd.value = false;
    clearAdd();
    await getProducts();
  }

  async function saveProducts() {
    try {
      if (editedproduct.value.product_id) {
        const res = await productService.updateProducts(
          editedproduct.value.product_id,
          editedproduct.value
        );
        clearAdd();
      } else {
        console.log(editedproduct.value);
        const res = await productService.saveProducts(editedproduct.value);
      }
      await getProducts();
    } catch (e) {
      console.log(e);
    }
  }

  function editProduct(product: Product) {
    editedproduct.value = JSON.parse(JSON.stringify(product));
    dialogEdit.value = true;
  }

  function clearAdd() {
    editedproduct.value = {
      name: "",
      price: 0,
      categoryId: 0,
      img: "no_img_available.jpg",
      files: [],
    };
  }

  async function deleteProduct(id: number) {
    try {
      const res = await productService.deleteProducts(id);
      await getProducts();
    } catch (e) {
      console.log(e);
    }
  }

  return {
    products,
    getProducts,
    dialogQRCode,
    dialogAdd,
    dialogEdit,
    dialogDelete,
    editProduct,
    deleteProduct,
    saveProducts,
    AddProduct,
    deleteproduct,
    editedproduct,
    clearAdd,
    page,
    take,
    keyword,
    lastPage,
    getAllProducts,
    category,
    getProductsByCategory,
    getByCatKeyword,
  };
});
