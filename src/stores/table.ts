import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Tables from "@/types/Table";
import tableService from "@/services/tables";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";
import { useOrderStore } from "./order";

export const useTableStore = defineStore("table", () => {
  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();
  const useOrder = useOrderStore();
  const page = ref(1);
  const take = ref(10);
  const lastPage = ref(0);
  const keyword = ref("");
  const dialogQRCode = ref(false);
  const dialogOpen = ref(false);
  const dialogAdd = ref(false);
  const dialogEdit = ref(false);
  const dialogDelete = ref(false);
  const deletetable = ref();
  const editedTable = ref<Tables>({ chair: 0, status: "ready" });
  const tables = ref<Tables[]>([]);

  watch(page, async (newPage, oldPage) => {
    await getTables();
  });

  watch(lastPage, async (newLastPage, oldLastPage) => {
    if (newLastPage < page.value) {
      page.value = 1;
    }
  });

  async function getTables() {
    loadingStore.isLoading = true;
    try {
      const res = await tableService.getTables({
        page: page.value,
        take: take.value,
      });
      tables.value = res.data.data;
      lastPage.value = res.data.lastPage;
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function getAllTables() {
    loadingStore.isLoading = true;
    try {
      const res = await tableService.getAllTables({
        keyword: keyword.value,
      });
      tables.value = res.data.data;
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function saveTable() {
    try {
      if (editedTable.value.id) {
        const res = await tableService.updateTables(
          editedTable.value.id,
          editedTable.value
        );
        await getAllTables();
      } else {
        console.log(editedTable.value);
        const res = await tableService.saveTables(editedTable.value);
        clearAdd();
        await getTables();
      }
    } catch (e) {
      console.log(e);
    }
  }

  const openTable = async (id: number, Tablevalue: Tables) => {
    loadingStore.isLoading = true;
    editedTable.value = Tablevalue;
    if (editedTable.value.status == "ready") {
      useOrder.dialogAdd = true;
    } else {
      messageStore.showError("Can't Open Table " + editedTable.value.id);
    }
    await getAllTables();
    loadingStore.isLoading = false;
  };

  // const movedTa

  function clearAdd() {
    editedTable.value = { chair: 0, status: "ready" };
  }

  async function deleteTable(id: number) {
    try {
      const res = await tableService.deleteTables(id);
      await getTables();
    } catch (e) {
      console.log(e);
    }
  }

  return {
    tables,
    clearAdd,
    saveTable,
    dialogQRCode,
    getTables,
    dialogAdd,
    editedTable,
    dialogEdit,
    dialogDelete,
    deleteTable,
    deletetable,
    lastPage,
    page,
    getAllTables,
    openTable,
    dialogOpen,
  };
});
