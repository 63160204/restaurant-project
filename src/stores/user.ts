import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import userService from "@/services/user";
import axios from "axios";

export const useUserStore = defineStore("user", () => {
  const dialogAdd = ref(false);
  const dialogEdit = ref(false);
  const dialogDelete = ref(false);
  const dialog = ref(false);
  const deleteuser = ref();
  const users = ref<User[]>([]);
  const editeduser = ref<User>({
    user_login: "",
    username: "",
    password: "",
  });
  const user = ref<User[]>([]);

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editeduser.value = { user_login: "", username: "", password: "" };
    }
  });

  async function AddUser() {
    try {
      const res = await userService.saveUser(editeduser.value);
    } catch (e) {
      console.log(e);
    }
    dialogAdd.value = false;
    clearAdd();
    await getUsers();
  }

  async function getUsers() {
    try {
      const res = await userService.getUsers();
      user.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function saveUser() {
    try {
      if (editeduser.value.user_id) {
        const res = await userService.updateUser(
          editeduser.value.user_id,
          editeduser.value
        );
      } else {
        console.log(editeduser.value);
        const res = await userService.saveUser(editeduser.value);
      }
      await getUsers();
    } catch (e) {
      console.log(e);
    }
  }

  function editUser(User: User) {
    editeduser.value = JSON.parse(JSON.stringify(User));
    dialog.value = true;
  }

  function editUser1(User: User) {
    editeduser.value = JSON.parse(JSON.stringify(User));
    dialogEdit.value = true;
  }

  function clearAdd() {
    editeduser.value = {
      user_login: "",
      username: "",
      password: "",
    };
  }

  async function deleteUser(id: number) {
    try {
      const res = await userService.deleteUser(id);
      await getUsers();
    } catch (e) {
      console.log(e);
    }
  }

  const login = (loginName: string, password: string): boolean => {
    const index = users.value.findIndex((item) => item.username === loginName);
    if (index >= 0) {
      const user = users.value[index];
      if (user.password === password) {
        return true;
      }
      return false;
    }
    return false;
  };

  const getPass = (id: number): string => {
    const index = users.value.findIndex((item) => item.user_id == id);
    console.log(index);
    if (index >= 0) {
      const user = users.value[index];
      console.log(user.password);
      return user.password;
    }
    return "";
  };

  return {
    user,
    getUsers,
    dialogAdd,
    dialogEdit,
    dialogDelete,
    editUser,
    editUser1,
    deleteUser,
    saveUser,
    AddUser,
    deleteuser,
    editeduser,
    clearAdd,
    dialog,
    login,
    getPass,
  };
});
