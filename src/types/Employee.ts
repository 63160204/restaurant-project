export default interface Employee {
  employee_id?: number;
  name: string;
  surname: string;
  tel: string;
  jobtitle: string;
}
