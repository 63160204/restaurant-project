import type OrderItem from "./OrderItem";
import type Tables from "./Table";

export default interface Order {
  order_id?: number;
  ord_amount?: number;
  ord_total?: number;
  ord_cash?: number;
  table?: Tables;
  tableId?: number;
  orderItem?: OrderItem[];
}
