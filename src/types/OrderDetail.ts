import type Product from "./Product";
import type OrderItem from "./OrderItem";
export default interface OrderDetail {
  order_detail_id?: number;
  ord_detail_name?: string;
  ord_detail_amount: number;
  ord_detail_total: number;
  ord_detail_price: number;
  ord_detail_status: string;
  ord_detail_table?: string;
  orderItems: OrderItem;
  products: Product;
}
