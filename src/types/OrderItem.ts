import type Order from "./Order";
import type OrderDetail from "./OrderDetail";
export default interface OrderItem {
  order_item_id?: number;
  ord_item_amount?: number;
  ord_item_total?: number;
  ore_item_status?: string;
  orderId?: number;
  order?: Order;
  orderDetail?: OrderDetail[];
}
