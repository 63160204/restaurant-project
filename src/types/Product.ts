import type Category from "./Category";

export default interface Products {
  product_id?: number;
  name: string;
  price: number;
  img?: string;
  categoryId: number;
  amount?: number;
  category?: Category;
}
