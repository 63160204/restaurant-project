export default interface User {
  user_id?: number;
  user_login: string;
  username: string;
  password: string;
}
